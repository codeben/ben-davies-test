import React, {Component} from 'react';
import {getData} from '../api';
import Vehicle from './Vehicle.js';

export default class VehicleList extends Component {

  constructor(props) {
    super(props);
    this.apiUrl = 'http://localhost:9988/api/vehicle';
    this.state = {
      data: null
    }
  }

  componentDidMount() {
    getData(this.apiUrl, (data) => {
      this.setState({
        data
      })
    });
  }

  populateVehicles(car) {
    return (
      <Vehicle key={car.id} car={car}/>
    )
  }

  render() {
    if (this.state.data) {

      const vehicleList = [];
      var cars = JSON.parse(this.state.data);
      for(var i = 0; i < cars.vehicles.length; i++) {
        vehicleList.push(this.populateVehicles(cars.vehicles[i]));
      }

      return (
        <div>
          <div className="vehicle-container">
            {vehicleList}
          </div>
        </div>
      )
    }

    return (<h1>Loading...</h1>);
  }
};