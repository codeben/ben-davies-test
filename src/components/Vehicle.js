import React, {Component, StyleSheet} from 'react';
import {getData} from '../api';

export default
class Vehicle extends Component {

  constructor(props) {
    super(props);

    this.apiUrl = this.props.car.url;

    this.state = {
      carData: null,
      popUpShown: false
    }
  }

  componentDidMount() {
    getData(this.apiUrl, (data) => {
      this.setState({
        carData: JSON.parse(data)
      })
    })
  }

  popUp() {
    this.setState({
      popUpShown: !this.state.popUpShown
    })
  }

  getMedia() {
    return this.props.car.media[0].url || '';
  }

  render() {

    if (this.state.carData) {

      var showMe = this.state.popUpShown ? ' vehicle-pop-up--show' : '';
      var showOverlay = this.state.popUpShown ? ' overlay--show' : '';

      var drivetrain = this.state.carData.meta.drivetrain.join(', ');
      var bodyStyles = this.state.carData.meta.bodystyles.join(', ');

      return (
        <div>
          <div onClick={() => this.popUp()} className="vehicle">
            <div className="vehicle__image">
              <img className="vehicle__image-element" src={this.getMedia()}/>
            </div>
            <div className="vehicle__information-area">
              <h3 className="vehicle__title">
                {this.props.car.id}
              </h3>
              <div className="vehicle__price">
                <p className="vehicle__price-text">
                  {'From ' + this.state.carData.price}
                </p>
              </div>
              <div className="vehicle__info">
                <p className="vehicle__info-text">
                  {this.state.carData.description}
                </p>
              </div>
            </div>
          </div>
          <div className={'vehicle-pop-up' + showMe}>
            <div className="vehicle-pop-up__close" onClick={() => this.popUp()}>Close</div>
            <div className="vehicle__information-area">
              <h3 className="vehicle__title">
                {this.props.car.id}
              </h3>
              <div className="vehicle__price">
                <p className="vehicle__price-text">
                  {'From ' + this.state.carData.price}
                </p>
              </div>
              <div className="vehicle__info">
                <p className="vehicle__info-text">
                  {this.state.carData.description}
                </p>
                <p className="vehicle__info-text vehicle__info-text--capitalize">
                  <strong className="vehicle__strong-text">Bodystyles:</strong>  {bodyStyles}
                </p>
                <p className="vehicle__info-text vehicle__info-text--capitalize">
                  <strong className="vehicle__strong-text">Drivetrain:</strong> {drivetrain}
                </p>
                <p className="vehicle__info-text vehicle__info-text--capitalize">
                  {this.state.carData.meta.emissions.template.replace('$value', this.state.carData.meta.emissions.value)}
                </p>
              </div>
            </div>
          </div>
          <div className={'overlay' + showOverlay}></div>
        </div>
      );
    } else {
      return null
    }
  }
}